#!/bin/bash
#
# pack original *.go sources into jar files before 'maven publish'
#

zip -u libtorrent-sources.jar -r src/ -x \*/.git/\* -y -x src/bitbucket.org/JakeKalstad/libtorrent-go

zip -d libtorrent-sources.jar libtorrent/\*

zip -u libtorrent-sources.jar -r src/bitbucket.org/JakeKalstad/libtorrent-go/*.go
